function drawLo() {
    var ctx = $('#chartLO');
    var chart = new Chart(ctx, {
        type: 'bar',
        data: {
            labels: [
                ['Audience-Cetered', 'Communication'],
                ['Synergistic', 'Collaboration'],
                ['Meta-level', 'Reflection'],
                ['Ethical leadership'],
                ['Social Problem', 'Solving'],
                ['Complex Thinking'],
                ['Adaptability', 'And Agibility'],
                ['Creativiy', 'And Innovation']
            ],
            datasets: [
                {
                    label: 'Previous Year',
                    data: [12, 12, 12, 12, 12, 12, 12, 12],
                    backgroundColor: '#22aa99'
                }, {
                    label: 'Current Year',
                    data: [1, 1, 0, 1, 1, 0, 0, 1],
                    backgroundColor: '#dc3912'
                }

            ]
        },
        options: {
            responsive: true,
            legend: {
                position: 'bottom'
            },
            scales: {
                xAxes: [{
                    stacked: true
                }],
                yAxes: [{
                    stacked: true,
                    ticks: {
                        suggestedMax: 15,
                        beginAtZero: true,
                        userCallback: function (value) {
                            switch (value) {
                                case 3:
                                    value = "Early"; break;
                                case 6:
                                    value = "Beginning"; break;
                                case 9:
                                    value = "Transition"; break;
                                case 12:
                                    value = "Developing"; break;
                                case 15:
                                    value = "Expert"
                                    break;
                                default:
                                    value = "";

                            }
                            return value;
                        },

                        stepSize: 3
                    }
                }]
            },
            tooltips: {
                // enabled: false,
                //position: 'nearest',
                //custom: customLotooltip
            }

        }
    });
}

function drawPDR() {
    var ctx = $('#chartPDR');
    var chart = new Chart(ctx, {
        type: 'horizontalBar',
        data: {
            labels: [
                ['Plan'],
                ['Do'],
                ['Review']
            ],
            datasets: [
                {
                    label: 'Previous Year',
                    data: [24, 24, 24],
                    backgroundColor: '#22aa99'
                }, {
                    label: 'Current Year',
                    data: [3, 4, 4],
                    backgroundColor: '#dc3912'
                }

            ]
        },
        options: {
            responsive: true,
            legend: {
                position: 'bottom'
            },
            scales: {
                xAxes: [{
                    stacked: true,
                    position: "top",
                    ticks: {
                        beginAtZero: true,
                        suggestedMax: 32,
                        userCallback: function (value) {
                            switch (value) {
                                case 4:
                                    value = "Early Preschool"; break;
                                case 8:
                                    value = "Preschool"; break;
                                case 12:
                                    value = "Grade K-1"; break;
                                case 16:
                                    value = "Grade 2-3"; break;
                                case 20:
                                    value = "Grade 4-5"; break;
                                case 24:
                                    value = "Grade 6-7"; break;
                                case 28:
                                    value = "Grade 8-9"; break;
                                case 32:
                                    value = "Grade 10-12"; break;
                                default:
                                    value = "";

                            }
                            return value;
                        },

                        stepSize: 4
                    }
                }],
                yAxes: [{
                    stacked: true,
                    ticks: {
                        beginAtZero: true,
                    }
                }]
            },

        }

    });
}


$(function () {
    drawLo();
    drawPDR();
    $(".subsubject td").click(function () {
        $("#subjectdetaildlg").modal();
    })
})

var customLotooltip = function (tooltip) {
    // Tooltip Element

    var tooltipEl = document.getElementById('LOTooltip');
    if (!tooltip) {
        tooltipEl.css({
            opacity: 0
        });
        return;
    }

    console.log(tooltip);
    tooltipEl.style.opacity = 1;
    tooltipEl.style.left = tooltip.x + 140 + 'px';
    tooltipEl.style.top = tooltip.y + 100 + 'px';
    tooltipEl.style.padding = tooltip.yPadding + 'px ' + tooltip.xPadding + 'px';

    // split out the label and value and make your own tooltip here


};