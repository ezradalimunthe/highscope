<?php
use yii\helpers\Html;

$this->registerJsFile(
    '@web/js/classroom/index.js',
    ['depends' => [\app\assets\AppAsset::className()]]
);
$this->title = "Class";
?>

<div class="card card-default">
    <div class="card-header card-header-border-bottom justify-content-between">
        <h2> Class K2-01</h2>
        <ul>
            <li>Date: May 10, 2019 </li>
            <li>Today Subject: <a href="#" style="color:green;"> Opinion And Thougts</a></li>
        </ul>
    </div>

    <div class="card-body">
        <div class="row">
            <?php for ($i=0; $i<20;$i++){?>
            <!--begin-->



            <div class="col-lg-6 col-xl-3">
                <div class="card card-default p-4">

                    <?= Html::img('@web/img/student.png',
                         ['alt' => 'Emma Smith', 'class'=>'mr-3 img-fluid rounded']) ?>

                    <div class="media-body text-dark">
                        <h5 class="mt-0 mb-2 text-dark">Emma Smith</h5>
                        <ul class="list-unstyled">
                          
                            <li class="d-flex mb-1">
                                <i class="mdi mdi-checkbox-blank-circle-outline mr-1"></i>
                                <span>View 
                                    <?=Html::a("Report Card",['classroom/reportcard']);?>

                                </span>
                            </li>
                            <li class="d-flex mb-1">
                                <i class="mdi mdi-checkbox-blank-circle-outline mr-1"></i>
                                <span>View Biodata</span>
                            </li>
                        </ul>
                        <button type="button" class="btn btn-primary btn-sm btn-block">Add Note</button>
                    </div>
                </div>
            </div>

            <!--end -->
            <?php }?>
        </div>

    </div>
</div>