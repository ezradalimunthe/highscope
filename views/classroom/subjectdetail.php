        <div class="modal fade" id="subjectdetaildlg" tabindex="-1" role="dialog" aria-labelledby="subjectdetaildlg"
            aria-hidden="true">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLarge">Subject Detail</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>  
                    <div class="modal-body">
                        <h2>Bahasa Indonesia 10</h2>
                        <table class="table">
                            <thead>
                                <tr>
                                    <th>Course Academic Standard</th>
                                    <th>Standard Grading</th>
                                    <th>Course Grading</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr class="bold">
                                    <td >Bahasa Indonesia 10</td>
                                    <td class="text-center"></td>
                                    <td class="text-center">B</td>
                                </tr>
                                <tr>
                                    <td>Standar Keterampilan Berbahasa Indonesia (SKBI)</td>
                                    <td class="text-center">B</td>
                                    <td class="text-center"></td>
                                </tr>
                                <tr>
                                    <td>Membaca</td>
                                    <td class="text-center">B</td>
                                    <td class="text-center"></td>
                                </tr>
                                <tr>
                                    <td>Menyimak</td>
                                    <td class="text-center">A</td>
                                    <td class="text-center"></td>
                                </tr>
                                <tr>
                                    <td>Menulis</td>
                                    <td class="text-center">C</td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td>Berbicara</td>
                                    <td class="text-center">A</td>
                                    <td></td>
                                </tr>
                            </tbody>

                        </table>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-danger btn-pill" data-dismiss="modal">Detail</button>
                        <button type="button" class="btn btn-danger btn-pill" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>
        </div>