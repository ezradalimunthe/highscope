<?php

use app\components\breadcrumb\BreadcrumbWidget;

$this->registerJsFile(
    '@web/plugins/charts/Chart.min.js',
    ['depends' => [\app\assets\AppAsset::className()]]
  );
$this->registerJsFile(
    '@web/js/classroom/index.js',
    ['depends' => [\app\assets\AppAsset::className()]]
  );
$this->title="Student Report"
?>
<div class="row">
    <div class="col-12">
        <?= BreadcrumbWidget::widget() ?>
    </div>
</div>


<div class="row mb-3">
    <div class="col-12">
        <div class="card">
            <div class="card-header d-flex justify-content-between bg-primary">

                <h2 class='text-white'>Transcript</h2>
                <div class="d-flex text-white">
                    <div class="mr-3">School Year:</div>
                    <div class="dropdown d-inline-block widget-dropdown ml-auto ">
                        <a class="dropdown-toggle text-white" href="#" role="button" data-toggle="dropdown"
                            aria-haspopup="true" aria-expanded="true" data-display="static">
                            2018-2019 Term 2
                        </a>
                        <ul class="dropdown-menu dropdown-menu-right" aria-labelledby="world-dropdown">
                            <li class="dropdown-item"><a href="#">2018-2019 Term 1</a></li>
                            <li class="dropdown-divider">&nbnsp;</li>
                            <li class="dropdown-item"><a href="#">Print</a></li>
                        </ul>
                    </div>

                </div>

            </div>
            <div class="card-body">

                <div class="row border mb-4 ml-1 mr-1">
                    <div class="col-6">
                        <div class="row mb-1 mt-1">
                            <div class="col-4">
                                Grade
                            </div>
                            <div class="col-8">
                                : 10
                            </div>
                        </div>
                        <div class="row mb-1">
                            <div class="col-4">
                                Major
                            </div>
                            <div class="col-8">
                                : Business And Information Technology
                            </div>
                        </div>
                        <div class="row mb-1">
                            <div class="col-4">
                                Total Credit
                            </div>
                            <div class="col-8">
                                : 8.5
                            </div>
                        </div>
                    </div>
                    <div class="col-6">
                        <div class="row mb-1 mt-1">
                            <div class="col-9 ">
                                Cumulative % Proficient Or Better
                            </div>
                            <div class="col-3">
                                : 92%
                            </div>
                        </div>
                        <div class="row mb-1">
                            <div class="col-9">
                                Cumulative % High Performance
                            </div>
                            <div class="col-3">
                                : 20%
                            </div>
                        </div>
                        <div class="row mb-1">
                            <div class="col-9">
                                GPE
                            </div>
                            <div class="col-3">
                                : 2.65
                            </div>
                        </div>
                    </div>
                </div>
                <table class="table table-hover table-responsive table-bordered reportcard">
                    <thead>
                        <tr>
                            <th>I</th>
                            <th>II</th>
                            <th>III</th>
                            <th>IV</th>
                            <th>V</th>
                        </tr>
                        <tr>
                            <th>Course Information</th>
                            <th>Course Number</th>
                            <th>Credit</th>
                            <th>Achievement</th>
                            <th>Academic Standing</th>
                        </tr>


                    </thead>
                    <tbody>
                        <tr>
                            <td colspan="5">Art And Music</td>
                        </tr>
                        <tr class="subsubject">
                            <td>Drawing And Painting</td>
                            <td>1064</td>
                            <td>0.5</td>
                            <td>C</td>
                            <td></td>
                        </tr>
                        <tr>
                            <td colspan="5">Business Information And Technology</td>
                        </tr>
                        <tr class="subsubject">
                            <td>Global Citizenship</td>
                            <td>1173</td>
                            <td>0.5</td>
                            <td>B</td>
                            <td></td>
                        </tr>
                        <tr class="subsubject">
                            <td>Introduction To Internship</td>
                            <td>1175</td>
                            <td>0.5</td>
                            <td>A</td>
                            <td></td>
                        </tr>

                        <tr class="subsubject">
                            <td>Multimedia I</td>
                            <td>1172</td>
                            <td>0.5</td>
                            <td>A</td>
                            <td></td>
                        </tr>
                        <tr class="subsubject">
                            <td>Small Business Management</td>
                            <td>1103</td>
                            <td>1</td>
                            <td>C</td>
                            <td></td>
                        </tr>
                        <tr>
                            <td colspan="5">Language Arts</td>
                        </tr>
                        <tr class="subsubject">
                            <td>Bahasa Indonesia 10</td>
                            <td>0276</td>
                            <td>1</td>
                            <td>A</td>
                            <td></td>
                        </tr>
                        <tr class="subsubject">
                            <td>English 10</td>
                            <td>0274</td>
                            <td>1</td>
                            <td>C</td>
                            <td></td>
                        </tr>
                        <tr>
                            <td colspan="5">Mathematics</td>
                        </tr>
                        <tr class="subsubject">
                            <td>Algebra 2</td>
                            <td>0117</td>
                            <td>1</td>
                            <td>C</td>
                            <td></td>
                        </tr>
                        <tr>
                            <td colspan="5">Physical Education</td>
                        </tr>
                        <tr class="subsubject">
                            <td>Sport Education</td>
                            <td>0255</td>
                            <td>0.5</td>
                            <td>B</td>
                            <td></td>
                        </tr>
                        <tr>
                            <td colspan="5">Religion</td>
                        </tr>
                        <tr class="subsubject">
                            <td>Islamic Studies 10</td>
                            <td>0171</td>
                            <td>0.5</td>
                            <td>B</td>
                            <td></td>
                        </tr>
                        <tr>
                            <td colspan="5">Science</td>
                        </tr>
                        <tr class="subsubject">
                            <td>Biology</td>
                            <td>0244</td>
                            <td>1</td>
                            <td>C</td>
                            <td></td>
                        </tr>
                        <tr>
                            <td colspan="5">Social Studies</td>
                        </tr>
                        <tr class="subsubject">
                            <td>Citizenship 1</td>
                            <td>1124</td>
                            <td>0.5</td>
                            <td>C</td>
                            <td></td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

<div class="row mb-3">
    <div class="col-12">
        <div class="card">
            <div class="card-header bg-primary">
                <h2 class="text-white">Learner Outcomes</h2>
            </div>
            <div class="card-body">
                <canvas id="chartLO"></canvas>
                <div id="LOTooltip" style="opacity: 0;
     position: absolute;
     max-width:200px;
     background:#FFFF99;
     color: black;
     padding: 3px;
     border-radius: 3px;
     -webkit-transition: all .1s ease;
     transition: all .1s ease;
     pointer-events: none;
     -webkit-transform: translate(-50%, 0);
     transform: translate(-50%, 0);">
                    <p style="font-size:14px;font-weight:bold">Synergistic Collaboration</p>
                    <p style="font-size:12px;font-weight:bold">Expert (9-12) Stage 1</p>
                    <p style="font-size:12px">Understands the expectation of her/his own role in achieving the group
                        goals complete the task with
                        quality and contributes ideas to surpass the expectation of the group's goal</p>
                </div>
            </div>

        </div>
    </div>
</div>

<div class="row mb-3">
    <div class="col-12">
        <div class="card">
            <div class="card-header bg-primary">
                <h2 class="text-white">Plan-Do-Review</h2>
            </div>
            <div class="card-body">
                <canvas id="chartPDR"></canvas>
            </div>

        </div>
    </div>
</div>
<div class="row mb-3">
    <div class="col-12">

        <div class="card ">
            <div class="card-header d-flex justify-content-between bg-primary">
                <h2 class='text-white'>Attendance Report</h2>
                <div class="d-flex text-white">
                    <div class="mr-3">School Year:</div>
                    <div class="dropdown d-inline-block widget-dropdown ml-auto ">
                        <a class="dropdown-toggle text-white" href="#" role="button" data-toggle="dropdown"
                            aria-haspopup="true" aria-expanded="true" data-display="static">
                            2018-2019 Term 1
                        </a>
                        <ul class="dropdown-menu dropdown-menu-right" aria-labelledby="world-dropdown">
                            <li class="dropdown-item"><a href="#">2018-2019 Term 2</a></li>
                            <li class="dropdown-divider">&nbnsp;</li>
                            <li class="dropdown-item"><a href="#">Print</a></li>
                        </ul>
                    </div>

                </div>

            </div>

            <div class="card-body">

                <table class="table table-hover text-center table-bordered">
                    <thead class="">
                        <tr>

                            <th>Description</th>
                            <th>Jul</th>
                            <th>Aug</th>
                            <th>Sep</th>
                            <th>Oct</th>
                            <th>Nov</th>
                            <th>Dec</th>
                            <th>Jan</th>
                            <th>Feb</th>
                            <th>Mar</th>
                            <th>Apr</th>
                            <th>May</th>
                            <th>June</th>
                        </tr>
                    </thead>
                    <tbody style="cursor:pointer">
                        <tr>
                            <td scope="row" class="text-right">Present</td>
                            <td class="text-center">100</td>
                            <td class="text-center">100</td>
                            <td class="text-center">100</td>
                            <td class="text-center">100</td>
                            <td class="text-center">100</td>
                            <td class="text-center">100</td>
                            <td class="text-center">100</td>
                            <td class="text-center">100</td>
                            <td class="text-center">100</td>
                            <td class="text-center">100</td>
                            <td class="text-center">100</td>
                            <td class="text-center">100</td>
                        </tr>
                        <tr>
                            <td scope="row" class="text-right">Tardy</td>
                            <td class="text-center">0</td>
                            <td class="text-center">0</td>
                            <td class="text-center">0</td>
                            <td class="text-center">0</td>
                            <td class="text-center">0</td>
                            <td class="text-center">0</td>
                            <td class="text-center">0</td>
                            <td class="text-center">0</td>
                            <td class="text-center">0</td>
                            <td class="text-center">0</td>
                            <td class="text-center">0</td>
                            <td class="text-center">0</td>
                        </tr>
                        <tr>
                            <td scope="row" class="text-right">Sick</td>
                            <td class="text-center">0</td>
                            <td class="text-center">0</td>
                            <td class="text-center">0</td>
                            <td class="text-center">0</td>
                            <td class="text-center">0</td>
                            <td class="text-center">0</td>
                            <td class="text-center">0</td>
                            <td class="text-center">0</td>
                            <td class="text-center">0</td>
                            <td class="text-center">0</td>
                            <td class="text-center">0</td>
                            <td class="text-center">0</td>
                        </tr>
                        <tr>
                            <td scope="row" class="text-right">Permisision Leave</td>
                            <td class="text-center">0</td>
                            <td class="text-center">0</td>
                            <td class="text-center">0</td>
                            <td class="text-center">0</td>
                            <td class="text-center">0</td>
                            <td class="text-center">0</td>
                            <td class="text-center">0</td>
                            <td class="text-center">0</td>
                            <td class="text-center">0</td>
                            <td class="text-center">0</td>
                            <td class="text-center">0</td>
                            <td class="text-center">0</td>
                        </tr>
                        <tr>
                            <td scope="row" class="text-right">Absent</td>
                            <td class="text-center">0</td>
                            <td class="text-center">0</td>
                            <td class="text-center">0</td>
                            <td class="text-center">0</td>
                            <td class="text-center">0</td>
                            <td class="text-center">0</td>
                            <td class="text-center">0</td>
                            <td class="text-center">0</td>
                            <td class="text-center">0</td>
                            <td class="text-center">0</td>
                            <td class="text-center">0</td>
                            <td class="text-center">0</td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>




<?=$this->render("subjectdetail");?>