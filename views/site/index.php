<?php

/* @var $this yii\web\View */

$this->title = 'Dashboard';
?>

<div class="alert alert-warning " role="alert">
    <i class="mdi mdi-bell-outline mr-1"></i>
    Next Class: <strong>Room 206 </strong> at <strong> 09:40</strong> about 14 minutes.
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">×</span>
    </button>
</div>

<div class="row">
    <div class="col-xl-6">
        <!-- To Do list -->
        <div class="card card-default todo-table" id="todo" data-scroll-height="550"
            style="height: 550px; overflow: hidden;">
            <div class="card-header justify-content-between">
                <h2>Subject For K2-01</h2>
            </div>
            <div class="slimScrollDiv" style="position: relative; overflow: hidden; width: auto; height: 100%;">
                <div class="card-body slim-scroll" style="overflow: hidden; width: auto; height: 100%;">
                    <div class="todo-single-item d-none" id="todo-input">
                        <form>
                            <div class="form-group">
                                <input type="text" class="form-control" placeholder="Enter Todo" autofocus="">
                            </div>
                        </form>
                    </div>
                    <div class="todo-list" id="todo-list">
                        <div class="todo-single-item d-flex flex-row justify-content-between finished">
                            <i class="mdi"></i>
                            <span>Subject 1: Lorem ipsum dolor sit amet</span>
                            <span class="badge badge-primary">Finished</span>
                        </div>
                        <div class="todo-single-item d-flex flex-row justify-content-between current">
                            <i class="mdi"></i>
                            <span>Subject 2: Opinion And Thoughts</span>
                            <span class="badge badge-primary">Today</span>
                        </div>
                        <div class="todo-single-item d-flex flex-row justify-content-between ">
                            <i class="mdi"></i>
                            <span> Subject 3: Lorem Ipsum </span>
                            <span class="badge badge-success">Dec 15, 2018</span>
                        </div>

                        <div class="todo-single-item d-flex flex-row justify-content-between ">
                            <i class="mdi"></i>
                            <span> Subject 3: Lorem Ipsum </span>
                            <span class="badge badge-success">Dec 15, 2018</span>
                        </div>

                        <div class="todo-single-item d-flex flex-row justify-content-between ">
                            <i class="mdi"></i>
                            <span> Subject 3: Lorem Ipsum </span>
                            <span class="badge badge-success">Dec 15, 2018</span>
                        </div>
                    </div>
                </div>
                <div class="slimScrollBar"
                    style="background: rgb(153, 153, 153) none repeat scroll 0% 0%; width: 5px; position: absolute; top: 0px; opacity: 0.4; display: none; border-radius: 7px; z-index: 99; right: 1px; height: 400.464px;">
                </div>
                <div class="slimScrollRail"
                    style="width: 5px; height: 100%; position: absolute; top: 0px; display: none; border-radius: 7px; background: rgb(51, 51, 51) none repeat scroll 0% 0%; opacity: 0.2; z-index: 90; right: 1px;">
                </div>
            </div>
            <div class="mt-3"></div>
        </div>
    </div>
    <div class="col-xl-6">
    <div class="card card-default" data-scroll-height="500" style="height: 500px; overflow: hidden;">
										<div class="card-header justify-content-between align-items-center card-header-border-bottom">
											<h2>Latest Notifications</h2>
											<div>
												<button class="text-black-50 mr-2 font-size-20"><i class="mdi mdi-cached"></i></button>
												<div class="dropdown show d-inline-block widget-dropdown">
													<a class="dropdown-toggle icon-burger-mini" href="#" role="button" id="dropdown-notification" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" data-display="static"></a>
													<ul class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdown-notification">
														<li class="dropdown-item"><a href="#">Action</a></li>
														<li class="dropdown-item"><a href="#">Another action</a></li>
														<li class="dropdown-item"><a href="#">Something else here</a></li>
													</ul>
												</div>
											</div>

										</div>
										<div class="slimScrollDiv" style="position: relative; overflow: hidden; width: auto; height: 100%;"><div class="card-body slim-scroll" style="overflow: hidden; width: auto; height: 100%;">
											<div class="media py-3 align-items-center justify-content-between">
												<div class="d-flex rounded-circle align-items-center justify-content-center mr-3 media-icon iconbox-45 bg-primary text-white">
													<i class="mdi mdi-pine-tree font-size-20"></i>
												</div>
												<div class="media-body pr-3 ">
													<a class="mt-0 mb-1 font-size-15 text-dark" href="#">Field Trip Program</a>
													<p>Remind students to join the field trip.</p>
												</div>
												<span class=" font-size-12 d-inline-block"><i class="mdi mdi-clock-outline"></i> 10 AM</span>
											</div>

											<div class="media py-3 align-items-center justify-content-between">
												<div class="d-flex rounded-circle align-items-center justify-content-center mr-3 media-icon iconbox-45 bg-success text-white">
													<i class="mdi mdi-email-outline font-size-20"></i>
												</div>
												<div class="media-body pr-3">
													<a class="mt-0 mb-1 font-size-15 text-dark" href="#">WIFI Password</a>
													<p>WIFI password has changed. Please contact the IT departement for new password</p>
												</div>
												<span class=" font-size-12 d-inline-block"><i class="mdi mdi-clock-outline"></i> 9 AM</span>
											</div>


											

										</div><div class="slimScrollBar" style="background: rgb(153, 153, 153) none repeat scroll 0% 0%; width: 5px; position: absolute; top: 0px; opacity: 0.4; display: none; border-radius: 7px; z-index: 99; right: 1px; height: 271.994px;"></div><div class="slimScrollRail" style="width: 5px; height: 100%; position: absolute; top: 0px; display: none; border-radius: 7px; background: rgb(51, 51, 51) none repeat scroll 0% 0%; opacity: 0.2; z-index: 90; right: 1px;"></div></div>
										<div class="mt-3"></div>
									</div>
    </div>
</div>
