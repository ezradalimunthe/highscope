<?php
 use yii\helpers\Html;
?>


<ul class="nav sidebar-inner" id="sidebar-menu">
    <li>
	<?=Html::a('<i class="mdi mdi-view-dashboard-outline"></i><span class="nav-text">Dashboard</span>', 
            ['site/index'],['class'=>['sidenav-item-link']]);?>


    </li>
    <li class="has-sub">
        <a class="sidenav-item-link" href="javascript:void(0)" data-toggle="collapse" data-target="#dashboard"
            aria-expanded="true" aria-controls="dashboard">
            <i class="mdi mdi-cube-outline"></i>
            <span class="nav-text">My Class</span>
            <b class="caret"></b>
        </a>
        <ul class="collapse" id="dashboard" data-parent="#sidebar-menu" style="">
            <div class="sub-menu">
			<li>
                    <?=Html::a('<span class="nav-text">K2-01</span>',
                    ['classroom/class/','c'=>'k2-01'], ['class'=>["sidenav-item-link"]]);?>

                </li>
                <li>
                    <?=Html::a('<span class="nav-text">K2-02</span>',
                    ['classroom/class/','c'=>'k2-02'], ['class'=>["sidenav-item-link"]]);?>

                </li>
            </div>
        </ul>
    </li>
    <li>
        <a class="sidenav-item-link" href="#">
            <i class="mdi mdi-book-multiple-variant"></i>
            <span class="nav-text">Student Index</span>
        </a>
    </li>
</ul>