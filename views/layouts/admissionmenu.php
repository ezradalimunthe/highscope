<?php
 use yii\helpers\Html;
?>

<ul class="nav sidebar-inner" id="sidebar-menu">
    <li>
        <?=Html::a('<i class="mdi mdi-view-dashboard-outline"></i><span class="nav-text">Dashboard</span>', 
            ['admissionform/index'],['class'=>['sidenav-item-link']]);?>

    </li>
    <li class="has-sub">
        <a class="sidenav-item-link" href="javascript:void(0)" data-toggle="collapse" data-target="#dashboard"
            aria-expanded="true" aria-controls="dashboard">
            <i class="mdi mdi-card-text-outline"></i>
            <span class="nav-text">Admission Form</span>
            <b class="caret"></b>
        </a>
        <ul class="collapse" id="dashboard" data-parent="#sidebar-menu" style="">
            <div class="sub-menu">
                <li>
                    <?=Html::a('<span class="nav-text">ECEP</span>',
                    ['admissionform/highschool'], ['class'=>["sidenav-item-link"]]);?>

                </li>
                <li>
                    <?=Html::a('<span class="nav-text">Elementary School</span>',
                    ['admissionform/highschool'], ['class'=>["sidenav-item-link"]]);?>

                </li>
                <li>
                    <?=Html::a('<span class="nav-text">Middle School</span>',
                    ['admissionform/highschool'], ['class'=>["sidenav-item-link"]]);?>

                </li>
                <li>
                    <?=Html::a('<span class="nav-text">High School</span>',
                    ['admissionform/highschool'], ['class'=>["sidenav-item-link"]]);?>

                </li>
            </div>
        </ul>
    </li>
    <li>
        <a class="sidenav-item-link" href="#">
            <i class="mdi mdi-checkbook"></i>
            <span class="nav-text">Submitted Form</span>
        </a>
    </li>


    <li>
        <a class="sidenav-item-link" href="#">
            <i class="mdi mdi-account-circle"></i>
            <span class="nav-text">Parent Data</span>
        </a>
    </li>

</ul>