<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="author" content="colorlib.com">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Sign Up Form</title>

    <link rel="stylesheet"
        href="https://colorlib.com/etc/bwiz/colorlib-wizard-16/fonts/material-icon/css/material-design-iconic-font.min.css">

    <link rel="stylesheet" href="https://colorlib.com/etc/bwiz/colorlib-wizard-16/css/style.css">
    <style>
    body {
        background-image: none;
    }

    input[type=checkbox] {
        width: 15px;
        height: 15px;
        cursor: pointer;
        border: 1px solid #000;

    }
    </style>
</head>

<body>
    <div class="main">
        <div class="container">
            <h2>Sign up to HighScope Indonesia </h2>
            <form method="POST" id="signup-form" class="signup-form" autocomplete="off">
                <h3>
                    <span class="title_text">Account Infomation</span>
                </h3>
                <fieldset>
                    <div class="fieldset-content">

                        <div class="form-group">
                            <label for="email" class="form-label">Email</label>
                            <input type="email" autocomplete="off" name="email" id="email1" placeholder="Your Email" />
                        </div>
                        <div class="form-group form-password">
                            <label for="password" class="form-label">Password</label>
                            <input type="password" autocomplete="off" name="password2" id="password2"
                                data-indicator="pwindicator" />
                            <div id="pwindicator">
                                <div class="bar-strength">
                                    <div class="bar-process">
                                        <div class="bar"></div>
                                    </div>
                                </div>
                                <div class="label"></div>
                            </div>
                        </div>
                        <div class="form-group form-password">
                            <label for="password1" class="form-label">Confirm Password</label>
                            <input type="password" autocomplete="off" name="password1" id="password1"
                                data-indicator="pwindicator" />
                            <div id="pwindicator">
                                <div class="bar-strength">
                                    <div class="bar-process">
                                        <div class="bar"></div>
                                    </div>
                                </div>
                                <div class="label"></div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="firsname" class="form-label">&nbsp;</label>
                            <input type="checkbox" autocomplete="off" />
                            <span>I have read and agree to the <a href="#">Terms</a></span>
                        </div>
                    </div>

                    <div class="fieldset-footer">
                        <span>Step 1 of 3</span>
                    </div>
                </fieldset>
                <h3>
                    <span class="title_text">Personal Information</span>
                </h3>
                <fieldset>
                    <div class="fieldset-content">
                        <div class="form-group">
                            <label for="firsname" class="form-label">First Name</label>
                            <input type="text" autocomplete="off" name="firstname" id="firstname"
                                placeholder="First Name" />
                        </div>
                        <div class="form-group">
                            <label for="firsname" class="form-label">Last Name</label>
                            <input type="text" autocomplete="off" name="lastname" id="lastname"
                                placeholder="Last Name" />
                        </div>
                        <div class="form-group">
                            <label for="firsname" class="form-label">Home Address</label>
                            <input type="text" autocomplete="off" name="Address" id="firstname"
                                placeholder="Home Address" />
                        </div>
                        <div class="form-group">
                            <label for="firsname" class="form-label">Post Code</label>
                            <input type="text" autocomplete="off" name="lastname" id="lastname"
                                placeholder="eg. 10110" />
                        </div>
                        <div class="form-group">
                            <label for="firsname" class="form-label">Mobile Phone</label>
                            <input type="text" autocomplete="off" name="lastname" id="lastname"
                                placeholder="Mobile Phone" />
                        </div>

                    </div>
                    <div class="fieldset-footer">
                        <span>Step 2 of 3</span>
                    </div>
                </fieldset>
                <h3>
                    <span class="title_text">Quick Survey</span>
                </h3>
                <fieldset>
                    <div class="fieldset-content">
                        <div class="form-group">
                            <label for="firsname" class="form-label">Question 1?</label>
                            <input type="text" autocomplete="off" name="lastname" id="lastname"
                                placeholder="" />
                        </div>
                        <div class="form-group">
                            <label for="firsname" class="form-label">Question 2?</label>
                            <input type="text" autocomplete="off" name="lastname" id="lastname"
                                placeholder="" />
                        </div>
                        <div class="form-group">
                            <label for="firsname" class="form-label">Question 3?</label>
                            <input type="text" autocomplete="off" name="lastname" id="lastname"
                                placeholder="" />
                        </div>
                        <div class="form-group">
                            <div class="form-textarea">
                                <label for="about_us" class="form-label">Where did you hear about us? </label>
                                <input type="text" autocomplete="off" name="lastname" id="lastname"
                                    placeholder="eg. Newspaper, Internet" />
                            </div>
                        </div>

                    </div>
                    <div class="fieldset-footer">
                        <span>Step 3 of 3</span>
                    </div>
                </fieldset>
               
            </form>
        </div>
    </div>

    <script src="https://colorlib.com/etc/bwiz/colorlib-wizard-16/vendor/jquery/jquery.min.js"
        type="06b1d2a6c3def7cff54c16bd-text/javascript"></script>
    <script src="https://colorlib.com/etc/bwiz/colorlib-wizard-16/vendor/jquery-validation/dist/jquery.validate.min.js"
        type="06b1d2a6c3def7cff54c16bd-text/javascript"></script>
    <script
        src="https://colorlib.com/etc/bwiz/colorlib-wizard-16/vendor/jquery-validation/dist/additional-methods.min.js"
        type="06b1d2a6c3def7cff54c16bd-text/javascript"></script>
    <script src="https://colorlib.com/etc/bwiz/colorlib-wizard-16/vendor/jquery-steps/jquery.steps.min.js"
        type="06b1d2a6c3def7cff54c16bd-text/javascript"></script>
    <script src="https://colorlib.com/etc/bwiz/colorlib-wizard-16/vendor/minimalist-picker/dobpicker.js"
        type="06b1d2a6c3def7cff54c16bd-text/javascript"></script>
    <script src="https://colorlib.com/etc/bwiz/colorlib-wizard-16/vendor/jquery.pwstrength/jquery.pwstrength.js"
        type="06b1d2a6c3def7cff54c16bd-text/javascript"></script>
    <script src="/js/admission/index.js" type="06b1d2a6c3def7cff54c16bd-text/javascript"></script>


    <script src="https://ajax.cloudflare.com/cdn-cgi/scripts/95c75768/cloudflare-static/rocket-loader.min.js"
        data-cf-settings="06b1d2a6c3def7cff54c16bd-|49" defer=""></script>
</body>

</html>