<div class="card">
    <div class="card-body">
        <h2 class="text-center">Sign up to great new account</h2>
        <form method="POST" id="signup-form" class="signup-form wizard clearfix" novalidate="novalidate"
            role="application">
            <div class="steps clearfix">
                <ul role="tablist">
                    <li role="tab" class="first current" aria-disabled="false" aria-selected="true"><a
                            id="signup-form-t-0" href="#signup-form-h-0" aria-controls="signup-form-p-0"><span
                                class="current-info audible"> </span>
                            <div class="title"><span class="number">1</span>
                                <span class="title_text">Account Infomation</span>
                            </div>
                        </a></li>
                    <li role="tab" class="done" aria-disabled="false" aria-selected="false"><a id="signup-form-t-1"
                            href="#signup-form-h-1" aria-controls="signup-form-p-1">
                            <div class="title"><span class="number">2</span>
                                <span class="title_text">Personal Information</span>
                            </div>
                        </a></li>
                    <li role="tab" class="disabled last" aria-disabled="true"><a id="signup-form-t-2"
                            href="#signup-form-h-2" aria-controls="signup-form-p-2">
                            <div class="title"><span class="number">3</span>
                                <span class="title_text">Payment Details</span>
                            </div>
                        </a></li>
                </ul>
            </div>
            <div class="content clearfix">
                <h3 id="signup-form-h-0" tabindex="-1" class="title current">
                    <span class="title_text">Account Infomation</span>
                </h3>
                <fieldset id="signup-form-p-0" role="tabpanel" aria-labelledby="signup-form-h-0" class="body current"
                    aria-hidden="false" style="left: 0px;">
                    <div class="fieldset-content">
                        <div class="form-group">
                            <label for="username" class="form-label">Username</label>
                            <label id="username-error" class="error" for="username"
                                style="display: none;"></label><input type="text" name="username" id="username"
                                placeholder="User Name" class="valid" aria-invalid="false">
                        </div>
                        <div class="form-group">
                            <label for="email" class="form-label">Email</label>
                            <label id="email-error" class="error" for="email" style="display: none;"></label><input
                                type="email" name="email" id="email" placeholder="Your Email" class="valid"
                                aria-invalid="false">
                        </div>
                        <div class="form-group form-password">
                            <label for="password" class="form-label">Password</label>
                            <input type="password" name="password" id="password" data-indicator="pwindicator"
                                class="valid" aria-invalid="false">
                            <div id="pwindicator" class="pw-mediocre">
                                <div class="bar-strength">
                                    <div class="bar-process">
                                        <div class="bar"></div>
                                    </div>
                                </div>
                                <div class="label">Medium Password <i class="zmdi zmdi-info"></i></div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="your_avatar" class="form-label">Select avatar</label>
                            <div class="form-file">
                                <input type="file" name="your_avatar" id="your_avatar" class="custom-file-input valid"
                                    aria-invalid="false">
                                <span id="val"></span>
                                <span id="button">Select File</span>
                            </div>
                        </div>
                    </div>
                    <div class="fieldset-footer">
                        <span>Step 1 of 3</span>
                    </div>
                </fieldset>
                <h3 id="signup-form-h-1" tabindex="-1" class="title">
                    <span class="title_text">Personal Information</span>
                </h3>
                <fieldset id="signup-form-p-1" role="tabpanel" aria-labelledby="signup-form-h-1" class="body"
                    style="left: 846px; display: none;" aria-hidden="true">
                    <div class="fieldset-content">
                        <div class="form-group">
                            <label for="full_name" class="form-label">Full name</label>
                            <input type="text" name="full_name" id="full_name" placeholder="Full Name">
                        </div>
                        <div class="form-select">
                            <label for="country" class="form-label">Country</label>
                            <select name="country" id="country">
                                <option value="">Country</option>
                                <option value="Australia">Australia</option>
                                <option value="USA">America</option>
                            </select>
                        </div>
                        <div class="form-radio">
                            <label for="gender" class="form-label">Gender</label>
                            <div class="form-radio-item">
                                <input type="radio" name="gender" value="male" id="male" checked="checked">
                                <label for="male">Male</label>
                                <input type="radio" name="gender" value="female" id="female">
                                <label for="female">Female</label>
                            </div>
                        </div>
                        <div class="form-textarea">
                            <label for="about_us" class="form-label">About us</label>
                            <textarea name="about_us" id="about_us" placeholder="Who are you ..."></textarea>
                        </div>
                    </div>
                    <div class="fieldset-footer">
                        <span>Step 2 of 3</span>
                    </div>
                </fieldset>
                <h3 id="signup-form-h-2" tabindex="-1" class="title">
                    <span class="title_text">Payment Details</span>
                </h3>
                <fieldset id="signup-form-p-2" role="tabpanel" aria-labelledby="signup-form-h-2" class="body"
                    style="display: none;" aria-hidden="true">
                    <div class="fieldset-content">
                        <div class="form-radio">
                            <label for="payment_type">Payment Type</label>
                            <div class="form-radio-flex">
                                <input type="radio" name="payment_type" id="payment_visa" value="payment_visa"
                                    checked="checked">
                                <label for="payment_visa"><img src="images/icon-visa.png" alt=""></label>
                                <input type="radio" name="payment_type" id="payment_master" value="payment_master">
                                <label for="payment_master"><img src="images/icon-master.png" alt=""></label>
                                <input type="radio" name="payment_type" id="payment_paypal" value="payment_paypal">
                                <label for="payment_paypal"><img src="images/icon-paypal.png" alt=""></label>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="form-group">
                                <label for="credit_card" class="form-label">Credit Card</label>
                                <input type="text" name="credit_card" id="credit_card">
                            </div>
                            <div class="form-group">
                                <label for="cvc" class="form-label">CVC</label>
                                <input type="text" name="cvc" id="cvc">
                            </div>
                        </div>
                        <div class="form-date">
                            <label for="expiry_date">Expiration Date</label>
                            <div class="form-flex">
                                <div class="form-date-item">
                                    <select id="expiry_date" name="expiry_date">
                                        <option value="">DD</option>
                                        <option value="01">01</option>
                                        <option value="02">02</option>
                                        <option value="03">03</option>
                                        <option value="04">04</option>
                                        <option value="05">05</option>
                                        <option value="06">06</option>
                                        <option value="07">07</option>
                                        <option value="08">08</option>
                                        <option value="09">09</option>
                                        <option value="10">10</option>
                                        <option value="11">11</option>
                                        <option value="12">12</option>
                                        <option value="13">13</option>
                                        <option value="14">14</option>
                                        <option value="15">15</option>
                                        <option value="16">16</option>
                                        <option value="17">17</option>
                                        <option value="18">18</option>
                                        <option value="19">19</option>
                                        <option value="20">20</option>
                                        <option value="21">21</option>
                                        <option value="22">22</option>
                                        <option value="23">23</option>
                                        <option value="24">24</option>
                                        <option value="25">25</option>
                                        <option value="26">26</option>
                                        <option value="27">27</option>
                                        <option value="28">28</option>
                                        <option value="29">29</option>
                                        <option value="30">30</option>
                                        <option value="31">31</option>
                                    </select>
                                    <span class="select-icon"><i class="zmdi zmdi-chevron-down"></i></span>
                                </div>
                                <div class="form-date-item">
                                    <select id="expiry_year" name="expiry_year">
                                        <option value="">YYYY</option>
                                        <option value="2019">2019</option>
                                        <option value="2018">2018</option>
                                        <option value="2017">2017</option>
                                        <option value="2016">2016</option>
                                        <option value="2015">2015</option>
                                        <option value="2014">2014</option>
                                        <option value="2013">2013</option>
                                        <option value="2012">2012</option>
                                        <option value="2011">2011</option>
                                        <option value="2010">2010</option>
                                        <option value="2009">2009</option>
                                        <option value="2008">2008</option>
                                        <option value="2007">2007</option>
                                        <option value="2006">2006</option>
                                        <option value="2005">2005</option>
                                        <option value="2004">2004</option>
                                        <option value="2003">2003</option>
                                        <option value="2002">2002</option>
                                        <option value="2001">2001</option>
                                        <option value="2000">2000</option>
                                        <option value="1999">1999</option>
                                        <option value="1998">1998</option>
                                        <option value="1997">1997</option>
                                        <option value="1996">1996</option>
                                        <option value="1995">1995</option>
                                        <option value="1994">1994</option>
                                        <option value="1993">1993</option>
                                        <option value="1992">1992</option>
                                        <option value="1991">1991</option>
                                        <option value="1990">1990</option>
                                        <option value="1989">1989</option>
                                        <option value="1988">1988</option>
                                        <option value="1987">1987</option>
                                        <option value="1986">1986</option>
                                        <option value="1985">1985</option>
                                        <option value="1984">1984</option>
                                        <option value="1983">1983</option>
                                        <option value="1982">1982</option>
                                        <option value="1981">1981</option>
                                        <option value="1980">1980</option>
                                        <option value="1979">1979</option>
                                        <option value="1978">1978</option>
                                        <option value="1977">1977</option>
                                        <option value="1976">1976</option>
                                        <option value="1975">1975</option>
                                        <option value="1974">1974</option>
                                        <option value="1973">1973</option>
                                        <option value="1972">1972</option>
                                        <option value="1971">1971</option>
                                        <option value="1970">1970</option>
                                        <option value="1969">1969</option>
                                        <option value="1968">1968</option>
                                        <option value="1967">1967</option>
                                        <option value="1966">1966</option>
                                        <option value="1965">1965</option>
                                        <option value="1964">1964</option>
                                        <option value="1963">1963</option>
                                        <option value="1962">1962</option>
                                        <option value="1961">1961</option>
                                        <option value="1960">1960</option>
                                        <option value="1959">1959</option>
                                        <option value="1958">1958</option>
                                        <option value="1957">1957</option>
                                        <option value="1956">1956</option>
                                        <option value="1955">1955</option>
                                        <option value="1954">1954</option>
                                        <option value="1953">1953</option>
                                        <option value="1952">1952</option>
                                        <option value="1951">1951</option>
                                        <option value="1950">1950</option>
                                        <option value="1949">1949</option>
                                        <option value="1948">1948</option>
                                        <option value="1947">1947</option>
                                        <option value="1946">1946</option>
                                        <option value="1945">1945</option>
                                        <option value="1944">1944</option>
                                        <option value="1943">1943</option>
                                        <option value="1942">1942</option>
                                        <option value="1941">1941</option>
                                        <option value="1940">1940</option>
                                        <option value="1939">1939</option>
                                        <option value="1938">1938</option>
                                        <option value="1937">1937</option>
                                        <option value="1936">1936</option>
                                        <option value="1935">1935</option>
                                        <option value="1934">1934</option>
                                        <option value="1933">1933</option>
                                        <option value="1932">1932</option>
                                        <option value="1931">1931</option>
                                        <option value="1930">1930</option>
                                        <option value="1929">1929</option>
                                        <option value="1928">1928</option>
                                        <option value="1927">1927</option>
                                        <option value="1926">1926</option>
                                        <option value="1925">1925</option>
                                        <option value="1924">1924</option>
                                        <option value="1923">1923</option>
                                        <option value="1922">1922</option>
                                        <option value="1921">1921</option>
                                        <option value="1920">1920</option>
                                        <option value="1919">1919</option>
                                        <option value="1918">1918</option>
                                        <option value="1917">1917</option>
                                        <option value="1916">1916</option>
                                        <option value="1915">1915</option>
                                        <option value="1914">1914</option>
                                        <option value="1913">1913</option>
                                        <option value="1912">1912</option>
                                        <option value="1911">1911</option>
                                        <option value="1910">1910</option>
                                        <option value="1909">1909</option>
                                        <option value="1908">1908</option>
                                        <option value="1907">1907</option>
                                        <option value="1906">1906</option>
                                        <option value="1905">1905</option>
                                        <option value="1904">1904</option>
                                        <option value="1903">1903</option>
                                        <option value="1902">1902</option>
                                        <option value="1901">1901</option>
                                        <option value="1900">1900</option>
                                        <option value="1899">1899</option>
                                    </select>
                                    <span class="select-icon"><i class="zmdi zmdi-chevron-down"></i></span>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="name_of_card" class="form-label">Name of card</label>
                            <input type="text" name="name_of_card" id="name_of_card">
                        </div>
                    </div>
                    <div class="fieldset-footer">
                        <span>Step 3 of 3</span>
                    </div>
                </fieldset>
            </div>
            <div class="actions clearfix">
                <ul role="menu" aria-label="Pagination">
                    <li class="disabled" aria-disabled="true"><a href="#previous" role="menuitem">Previous</a></li>
                    <li aria-hidden="false" aria-disabled="false"><a href="#next" role="menuitem">Next</a></li>
                    <li style="display: none;" aria-hidden="true"><a href="#finish" role="menuitem">Submit</a></li>
                </ul>
            </div>
        </form>
    </div>
</div>

<style>
display-flex,
.form-group,
.form-select,
.form-radio,
.form-textarea,
.form-date,
.form-radio-item,
.form-flex,
.form-date-item .select-icon,
.form-date-item .select-icon i,
#val,
#button,
.steps ul,
.actions ul,
.actions ul li a,
.title,
.number {
    display: flex;
    display: -webkit-flex
}

list-type-ulli,
ul,
.actions ul {
    list-style-type: none;
    margin: 0;
    padding: 0
}

@font-face {
    font-family: roboto slab;
    font-style: normal;
    font-weight: 300;
    src: url(../fonts/roboto-slab/roboto-slab-v7-latin-300.eot);
    src: local("Roboto Slab Light"),
        local("RobotoSlab-Light"),
        url(../fonts/roboto-slab/roboto-slab-v7-latin-300.eot?#iefix) format("embedded-opentype"),
        url(../fonts/roboto-slab/roboto-slab-v7-latin-300.woff2) format("woff2"),
        url(../fonts/roboto-slab/roboto-slab-v7-latin-300.woff) format("woff"),
        url(../fonts/roboto-slab/roboto-slab-v7-latin-300.ttf) format("truetype"),
        url(../fonts/roboto-slab/roboto-slab-v7-latin-300.svg#RobotoSlab) format("svg")
}

@font-face {
    font-family: roboto slab;
    font-style: normal;
    font-weight: 400;
    src: url(../fonts/roboto-slab/roboto-slab-v7-latin-regular.eot);
    src: local("Roboto Slab Regular"),
        local("RobotoSlab-Regular"),
        url(../fonts/roboto-slab/roboto-slab-v7-latin-regular.eot?#iefix) format("embedded-opentype"),
        url(../fonts/roboto-slab/roboto-slab-v7-latin-regular.woff2) format("woff2"),
        url(../fonts/roboto-slab/roboto-slab-v7-latin-regular.woff) format("woff"),
        url(../fonts/roboto-slab/roboto-slab-v7-latin-regular.ttf) format("truetype"),
        url(../fonts/roboto-slab/roboto-slab-v7-latin-regular.svg#RobotoSlab) format("svg")
}

@font-face {
    font-family: roboto slab;
    font-style: normal;
    font-weight: 700;
    src: url(../fonts/roboto-slab/roboto-slab-v7-latin-700.eot);
    src: local("Roboto Slab Bold"),
        local("RobotoSlab-Bold"),
        url(../fonts/roboto-slab/roboto-slab-v7-latin-700.eot?#iefix) format("embedded-opentype"),
        url(../fonts/roboto-slab/roboto-slab-v7-latin-700.woff2) format("woff2"),
        url(../fonts/roboto-slab/roboto-slab-v7-latin-700.woff) format("woff"),
        url(../fonts/roboto-slab/roboto-slab-v7-latin-700.ttf) format("truetype"),
        url(../fonts/roboto-slab/roboto-slab-v7-latin-700.svg#RobotoSlab) format("svg")
}

a:focus,
a:active {
    text-decoration: none;
    outline: none;
    transition: all 300ms ease 0s;
    -moz-transition: all 300ms ease 0s;
    -webkit-transition: all 300ms ease 0s;
    -o-transition: all 300ms ease 0s;
    -ms-transition: all 300ms ease 0s
}

input,
select,
textarea {
    outline: none;
    appearance: unset !important;
    -moz-appearance: unset !important;
    -webkit-appearance: unset !important;
    -o-appearance: unset !important;
    -ms-appearance: unset !important
}

input::-webkit-outer-spin-button,
input::-webkit-inner-spin-button {
    appearance: none !important;
    -moz-appearance: none !important;
    -webkit-appearance: none !important;
    -o-appearance: none !important;
    -ms-appearance: none !important;
    margin: 0
}

input:focus,
select:focus,
textarea:focus {
    outline: none;
    box-shadow: none !important;
    -moz-box-shadow: none !important;
    -webkit-box-shadow: none !important;
    -o-box-shadow: none !important;
    -ms-box-shadow: none !important
}

input[type=checkbox] {
    appearance: checkbox !important;
    -moz-appearance: checkbox !important;
    -webkit-appearance: checkbox !important;
    -o-appearance: checkbox !important;
    -ms-appearance: checkbox !important
}

input[type=radio] {
    appearance: radio !important;
    -moz-appearance: radio !important;
    -webkit-appearance: radio !important;
    -o-appearance: radio !important;
    -ms-appearance: radio !important
}

input:-webkit-autofill {
    box-shadow: 0 0 0 30px transparent inset;
    -moz-box-shadow: 0 0 0 30px transparent inset;
    -webkit-box-shadow: 0 0 0 30px transparent inset;
    -o-box-shadow: 0 0 0 30px transparent inset;
    -ms-box-shadow: 0 0 0 30px transparent inset;
    background-color: transparent !important
}

.clear {
    clear: both
}

h2 {
    font-size: 20px;
    color: #222;
    font-weight: 700;
    text-transform: uppercase;
    text-align: center;
    margin: 0;
    padding-top: 35px
}

body {
    font-size: 14px;
    line-height: 1.6;
    color: #222;
    font-weight: 400;
    font-family: roboto slab;
    margin: 0;
    background-image: url(../images/body-bg.jpg);
    background-repeat: no-repeat;
    background-size: cover;
    -moz-background-size: cover;
    -webkit-background-size: cover;
    -o-background-size: cover;
    -ms-background-size: cover;
    background-position: center center
}

.main {
    padding: 95px 0
}

.container {
    width: 850px;
    position: relative;
    margin: 0 auto;
    background: #fff;
    box-shadow: 0 5px 9.5px .5px rgba(0, 0, 0, .1);
    -moz-box-shadow: 0 5px 9.5px .5px rgba(0, 0, 0, .1);
    -webkit-box-shadow: 0 5px 9.5px .5px rgba(0, 0, 0, .1);
    -o-box-shadow: 0 5px 9.5px .5px rgba(0, 0, 0, .1);
    -ms-box-shadow: 0 5px 9.5px .5px rgba(0, 0, 0, .1);
    border-radius: 10px;
    -moz-border-radius: 10px;
    -webkit-border-radius: 10px;
    -o-border-radius: 10px;
    -ms-border-radius: 10px
}

.signup-form {
    padding: 0 20px;
    position: relative
}

.form-group,
.form-select,
.form-radio,
.form-textarea,
.form-date {
    align-items: center;
    -moz-align-items: center;
    -webkit-align-items: center;
    -o-align-items: center;
    -ms-align-items: center;
    margin-bottom: 30px;
    position: relative
}

label {
    display: inline-block;
    width: 155px;
    text-align: right;
    margin-right: 27px
}

label.error {
    position: absolute;
    right: 92px;
    font-size: 11px;
    color: red
}

label.error i {
    font-size: 12px
}

input.error {
    border: 1px solid red
}

input,
select,
textarea {
    box-sizing: border-box;
    background: 0 0;
    font-size: 14px;
    padding: 15px 20px;
    border: 1px solid #ebebeb;
    font-family: roboto slab;
    border-radius: 5px;
    -moz-border-radius: 5px;
    -webkit-border-radius: 5px;
    -o-border-radius: 5px;
    -ms-border-radius: 5px;
    display: block;
    width: 345px;
    margin-right: 20px;
    font-weight: 700
}

input:focus,
select:focus,
textarea:focus {
    background: 0 0
}

input::-webkit-input-placeholder,
select::-webkit-input-placeholder,
textarea::-webkit-input-placeholder {
    color: #999;
    font-weight: 400
}

input::-moz-placeholder,
select::-moz-placeholder,
textarea::-moz-placeholder {
    color: #999;
    font-weight: 400
}

input:-ms-input-placeholder,
select:-ms-input-placeholder,
textarea:-ms-input-placeholder {
    color: #999;
    font-weight: 400
}

input:-moz-placeholder,
select:-moz-placeholder,
textarea:-moz-placeholder {
    color: #999;
    font-weight: 400
}

select {
    color: #999;
    font-weight: 400
}

.form-textarea textarea {
    height: 150px;
    width: 534px
}

.form-radio {
    margin-bottom: 40px
}

.form-radio-item input {
    width: 0;
    height: 0;
    position: absolute;
    left: -9999px
}

.form-radio-item input+label {
    margin: 0;
    padding: 12px 10px;
    width: 85px;
    height: 50px;
    box-sizing: border-box;
    position: relative;
    display: inline-block;
    text-align: center;
    background-color: #f8f8f8;
    font-size: 14px;
    font-weight: 400;
    color: #f8f8f8;
    text-align: center;
    text-transform: none;
    transition: border-color .15s ease-out, color .25s ease-out, background-color .15s ease-out, box-shadow .15s ease-out;
    border-radius: 5px;
    -moz-border-radius: 5px;
    -webkit-border-radius: 5px;
    -o-border-radius: 5px;
    -ms-border-radius: 5px
}

.form-radio-item input:checked+label {
    background-color: #1abc9c;
    color: #fff;
    z-index: 1
}

.form-radio-item input:focus+label {
    outline: none
}

.form-radio-item input:hover {
    background-color: #1abc9c;
    color: #fff
}

.form-radio-item input+label:first-of-type {
    border-radius: 5px 0 0 5px;
    -moz-border-radius: 5px 0 0 5px;
    -webkit-border-radius: 5px 0 0 5px;
    -o-border-radius: 5px 0 0 5px;
    -ms-border-radius: 5px 0 0 5px;
    border-right: none
}

.form-radio-item input+label:last-of-type {
    border-radius: 0 5px 5px 0;
    -moz-border-radius: 0 5px 5px 0;
    -webkit-border-radius: 0 5px 5px 0;
    -o-border-radius: 0 5px 5px 0;
    -ms-border-radius: 0 5px 5px 0;
    border-left: none
}

.form-radio-flex input {
    width: 0;
    height: 0;
    position: absolute;
    left: -9999px
}

.form-radio-flex input+label {
    margin: 0;
    padding: 12px 10px;
    width: 86px;
    height: 50px;
    box-sizing: border-box;
    position: relative;
    display: inline-block;
    text-align: center;
    background-color: transparent;
    border: 1px solid #ebebeb;
    text-align: center;
    text-transform: none;
    transition: border-color .15s ease-out, color .25s ease-out, background-color .15s ease-out, box-shadow .15s ease-out;
    border-radius: 5px;
    -moz-border-radius: 5px;
    -webkit-border-radius: 5px;
    -o-border-radius: 5px;
    -ms-border-radius: 5px
}

.form-radio-flex input:checked+label {
    border: 1px solid #1abc9c;
    z-index: 1
}

.form-radio-flex input:focus+label {
    outline: none
}

.form-radio-flex input:hover {
    border: 1px solid #1abc9c
}

#signup-form-p-2 .form-radio-flex,
#signup-form-p-2 .form-flex,
#signup-form-p-2 .form-group input {
    width: 534px
}

#signup-form-p-2 #credit_card {
    width: 275px
}

#signup-form-p-2 #cvc {
    width: 143px
}

.form-row {
    padding-left: 80px
}

.form-row .form-group label {
    width: auto
}

.form-row .form-group:first-child {
    float: left
}

.form-row .form-group:last-child {
    float: right
}

.form-date {
    clear: both
}

.form-date-item {
    position: relative;
    overflow: hidden
}

.form-date-item:last-child:after {
    width: 0
}

.form-date-item select {
    position: relative;
    background: 0 0;
    z-index: 10;
    cursor: pointer;
    margin-right: 10px
}

.form-date-item #expiry_date {
    width: 86px
}

.form-date-item #expiry_year {
    width: 104px
}

.form-date-item .select-icon {
    z-index: 0;
    position: absolute;
    top: 0;
    right: 10px;
    bottom: 0;
    justify-content: center;
    -moz-justify-content: center;
    -webkit-justify-content: center;
    -o-justify-content: center;
    -ms-justify-content: center;
    align-items: center;
    -moz-align-items: center;
    -webkit-align-items: center;
    -o-align-items: center;
    -ms-align-items: center
}

.form-date-item .select-icon i {
    justify-content: center;
    -moz-justify-content: center;
    -webkit-justify-content: center;
    -o-justify-content: center;
    -ms-justify-content: center;
    align-items: center;
    -moz-align-items: center;
    -webkit-align-items: center;
    -o-align-items: center;
    -ms-align-items: center;
    width: 30px;
    height: 20px;
    font-size: 22px;
    color: #999
}

.custom-file-input::-webkit-file-upload-button {
    visibility: hidden
}

input[type=file] {
    height: 50px;
    opacity: 0
}

#val {
    width: 345px;
    height: 50px;
    position: absolute;
    top: 0;
    left: 0;
    font-size: 14px;
    font-weight: 700;
    pointer-events: none;
    border: 1px solid #ebebeb;
    font-family: roboto slab;
    border-radius: 5px;
    -moz-border-radius: 5px;
    -webkit-border-radius: 5px;
    -o-border-radius: 5px;
    -ms-border-radius: 5px;
    align-items: center;
    -moz-align-items: center;
    -webkit-align-items: center;
    -o-align-items: center;
    -ms-align-items: center;
    justify-content: center;
    -moz-justify-content: center;
    -webkit-justify-content: center;
    -o-justify-content: center;
    -ms-justify-content: center
}

#button {
    cursor: pointer;
    width: 130px;
    background: #f8f8f8;
    height: 50px;
    color: #999;
    position: absolute;
    right: 0;
    top: 0;
    font-size: 14px;
    text-align: center;
    -webkit-transition: 500ms all;
    -moz-transition: 500ms all;
    transition: 500ms all;
    align-items: center;
    -moz-align-items: center;
    -webkit-align-items: center;
    -o-align-items: center;
    -ms-align-items: center;
    justify-content: center;
    -moz-justify-content: center;
    -webkit-justify-content: center;
    -o-justify-content: center;
    -ms-justify-content: center
}

#button:hover {
    background-color: #dfdfdf
}

.form-file {
    width: 488px;
    position: relative
}

.form-password {
    position: relative
}

.bar-strength {
    width: 60px;
    height: 15px;
    border-radius: 8px;
    -moz-border-radius: 8px;
    -webkit-border-radius: 8px;
    -o-border-radius: 8px;
    -ms-border-radius: 8px;
    background: #f8f8f8;
    position: absolute;
    right: 193px;
    top: 50%;
    transform: translateY(-50%);
    -moz-transform: translateY(-50%);
    -webkit-transform: translateY(-50%);
    -o-transform: translateY(-50%);
    -ms-transform: translateY(-50%)
}

.bar-process {
    width: 100%;
    height: 100%;
    position: relative
}

.bar {
    position: absolute;
    left: 0;
    top: 0;
    height: 100%;
    border-radius: 8px;
    -moz-border-radius: 8px;
    -webkit-border-radius: 8px;
    -o-border-radius: 8px;
    -ms-border-radius: 8px
}

.pw-very-weak .bar {
    background: #d00;
    width: 25px
}

.pw-very-weak .label,
.pw-weak .label {
    color: #d00
}

.pw-weak .bar {
    background: #d00;
    width: 25px
}

.pw-mediocre .bar {
    background: #fca812;
    width: 35px
}

.pw-strong .bar {
    background: #fca812;
    width: 42px
}

.pw-mediocre .label,
.pw-strong .label {
    color: #fca812
}

.pw-very-strong .bar {
    background: #1abc9c;
    width: 100%
}

.pw-very-strong .label {
    color: #1abc9c
}

.label {
    font-size: 11px
}

.label i {
    font-size: 12px
}

fieldset {
    border: none;
    padding: 0;
    margin: 0
}

.steps {
    border-top: 1px solid #ebebeb;
    border-bottom: 1px solid #ebebeb;
    padding: 12px 20px
}

.steps ul {
    justify-content: space-around;
    -moz-justify-content: space-around;
    -webkit-justify-content: space-around;
    -o-justify-content: space-around;
    -ms-justify-content: space-around
}

.steps ul li {
    padding-right: 80px;
    padding-left: 0;
    position: relative
}

.steps ul li:after {
    position: absolute;
    width: 1px;
    height: 30px;
    content: "";
    background: #ebebeb;
    right: 18px;
    top: 50%;
    transform: translateY(-50%);
    -moz-transform: translateY(-50%);
    -webkit-transform: translateY(-50%);
    -o-transform: translateY(-50%);
    -ms-transform: translateY(-50%)
}

.steps ul li a {
    color: #999;
    text-decoration: none;
    font-weight: 700
}

.steps ul li:last-child:after {
    width: 0
}

.steps ul .current a {
    color: #222
}

.steps ul .current a .number {
    border: 2px solid #222
}

.steps ul .done a {
    color: #1abc9c
}

.steps ul .done a .number {
    position: relative;
    font-size: 0;
    border: 2px solid #1abc9c
}

.steps ul .done a .number:after {
    position: absolute;
    left: 50%;
    top: 50%;
    transform: translate(-50%, -50%);
    -moz-transform: translate(-50%, -50%);
    -webkit-transform: translate(-50%, -50%);
    -o-transform: translate(-50%, -50%);
    -ms-transform: translate(-50%, -50%);
    font-size: 14px;
    color: #1abc9c;
    font-family: material-design-iconic-font;
    content: '\f26b';
    font-weight: 700
}

.actions {
    position: absolute;
    bottom: 14px;
    width: 100%;
    padding: 0 20px;
    right: 0
}

.actions ul {
    align-items: center;
    -moz-align-items: center;
    -webkit-align-items: center;
    -o-align-items: center;
    -ms-align-items: center;
    justify-content: flex-end;
    -moz-justify-content: flex-end;
    -webkit-justify-content: flex-end;
    -o-justify-content: flex-end;
    -ms-justify-content: flex-end;
    width: 100%
}

.actions ul .disabled {
    display: none
}

.actions ul li {
    margin-left: 10px
}

.actions ul li:first-child a {
    background: #f8f8f8;
    color: #999
}

.actions ul li:first-child a:hover {
    background-color: #dfdfdf
}

.actions ul li a {
    width: 150px;
    height: 50px;
    color: #fff;
    background: #1abc9c;
    align-items: center;
    -moz-align-items: center;
    -webkit-align-items: center;
    -o-align-items: center;
    -ms-align-items: center;
    justify-content: center;
    -moz-justify-content: center;
    -webkit-justify-content: center;
    -o-justify-content: center;
    -ms-justify-content: center;
    text-decoration: none;
    border-radius: 5px;
    -moz-border-radius: 5px;
    -webkit-border-radius: 5px;
    -o-border-radius: 5px;
    -ms-border-radius: 5px
}

.actions ul li a:hover {
    background-color: #148f77
}

.content {
    overflow: hidden
}

.content .current {
    padding-top: 60px
}

.content h3 {
    display: none
}

.fieldset-content {
    height: 465px;
    border-bottom: 1px solid #ebebeb;
    padding-right: 110px
}

.fieldset-footer {
    padding: 28px 0
}

.fieldset-footer span {
    color: #999
}

.title {
    align-items: center;
    -moz-align-items: center;
    -webkit-align-items: center;
    -o-align-items: center;
    -ms-align-items: center
}

.number {
    width: 31px;
    height: 31px;
    border-radius: 50%;
    -moz-border-radius: 50%;
    -webkit-border-radius: 50%;
    -o-border-radius: 50%;
    -ms-border-radius: 50%;
    align-items: center;
    -moz-align-items: center;
    -webkit-align-items: center;
    -o-align-items: center;
    -ms-align-items: center;
    justify-content: center;
    -moz-justify-content: center;
    -webkit-justify-content: center;
    -o-justify-content: center;
    -ms-justify-content: center;
    border: 2px solid #999;
    margin-right: 15px
}

@media screen and (max-width:992px) {
    .container {
        width: calc(100% - 40px);
        max-width: 100%
    }

    .bar-strength {
        display: none
    }
}

@media screen and (max-width:768px) {

    .steps ul,
    .form-row {
        flex-direction: column;
        -moz-flex-direction: column;
        -webkit-flex-direction: column;
        -o-flex-direction: column;
        -ms-flex-direction: column
    }

    .steps ul li:after {
        width: 0
    }

    .title {
        margin-bottom: 20px;
        margin-left: auto;
        margin-right: auto
    }

    .form-row .form-date,
    .form-row .form-select,
    .form-file,
    input,
    select,
    textarea,
    .form-textarea textarea,
    #signup-form-p-2 #credit_card,
    #signup-form-p-2 #cvc,
    #signup-form-p-2 .form-radio-flex,
    #signup-form-p-2 .form-flex,
    #signup-form-p-2 .form-group input,
    .form-row .form-group label {
        width: 100%
    }

    .form-row .form-group:first-child,
    .form-row .form-group:last-child {
        float: none
    }

    input,
    select,
    textarea {
        margin-right: 0
    }

    .fieldset-content {
        padding-right: 0;
        height: 600px
    }

    .form-group,
    .form-select,
    .form-radio,
    .form-textarea,
    .form-date {
        flex-direction: column;
        -moz-flex-direction: column;
        -webkit-flex-direction: column;
        -o-flex-direction: column;
        -ms-flex-direction: column
    }

    label {
        width: 100%;
        text-align: left;
        margin-right: 0;
        margin-bottom: 15px
    }

    .form-row {
        padding-left: 0
    }
}

@media screen and (max-width:600px) {
    #val {
        width: 99%
    }

    #button {
        top: 60px;
        width: 100%
    }

    .fieldset-footer {
        padding: 78px 0
    }

    .actions ul li a {
        width: 100px
    }

    .steps ul li {
        padding-right: 0
    }
}

@media screen and (max-width:480px) {
    .signup-form {
        padding-left: 30px;
        padding-right: 30px
    }

    .form-radio-flex input+label {
        margin-bottom: 20px
    }

    .form-flex {
        flex-direction: column;
        -moz-flex-direction: column;
        -webkit-flex-direction: column;
        -o-flex-direction: column;
        -ms-flex-direction: column
    }

    .form-date-item #expiry_date,
    .form-date-item #expiry_year {
        width: 100%;
        margin-bottom: 10px
    }

    .fieldset-content {
        height: 750px
    }
}
</style>