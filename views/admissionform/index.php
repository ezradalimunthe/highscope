<?php
$this->title = "Dashboard";
?>
<div class="row">
    <div class="col-12">
        <div class="alert alert-info" role="alert">
            Welcome to Highscope! Please select Admission Form below to proceed.
        </div>
    </div>
</div>

<div class="card card-default">
    <div class="card-header card-header-border-bottom">
        <h2>Academic Program Admission Form</h2>
    </div>
    <div class="card-body">
        <div class="card-deck">
            <div class="card">
                <img class="card-img-top" src="http://localhost:9091/img/ecep.jpg" alt="Card image cap">
                <div class="card-body">
                    <h5 class="card-title text-primary">Early Childhood Educational Program</h5>
                   
                    <p class="card-text">
                       <button class="btn btn-info">Enroll</button>
                    </p>
                </div>
            </div>
            <div class="card">
                <img class="card-img-top" src="http://localhost:9091/img/es.jpg" alt="Card image cap">
                <div class="card-body">
                    <h5 class="card-title text-primary">Elementary School</h5>
                   
                   
                    <p class="card-text">
                       <button class="btn btn-info">Enroll</button>
                    </p>
                </div>
            </div>
            <div class="card">
                <img class="card-img-top" src="http://localhost:9091/img/ms.jpg" alt="Card image cap">
                <div class="card-body">
                    <h5 class="card-title text-primary">Middle School</h5>
                   
                    <p class="card-text">
                       <button class="btn btn-info">Enroll</button>
                    </p>
                </div>
            </div>
            
            <div class="card">
                <img class="card-img-top" src="http://localhost:9091/img/hs.jpg" alt="Card image cap">
                <div class="card-body">
                    <h5 class="card-title text-primary">High School</h5>
                    
                    
                    <p class="card-text">
                       <button class="btn btn-info">Enroll</button>
                    </p>
                </div>
            </div>
        </div>
    </div>
</div>