<?php
/* @var $this yii\web\View */
$this->title="Admission Form";
?>
<div class="card">
    <div class="card-header card-header-border-bottom bg-primary d-flex justify-content-between">
        <h2 class="text-white">APPLICATION FOR ADMISSION
            High School</h2>

    </div>
    <div class="card-body">
        <ul class="nav nav-tabs" id="myTab" role="tablist">
            <li class="nav-item">
                <a class="nav-link active" id="icon-home-tab" data-toggle="tab" href="#tab1">
                    <i class="mdi mdi-account mr-1"></i> Student Profile</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" id="icon-profile-tab" data-toggle="tab" href="#tab2">
                    <i class="mdi mdi-account-multiple mr-1"></i> Family</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" id="icon-contact-tab" data-toggle="tab" href="#tab3">
                    <i class="mdi mdi-checkbook mr-1"></i> Other</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" id="icon-contact-tab" data-toggle="tab" href="#tab4">
                    <i class="mdi mdi-attachment"></i> Documents</a>
            </li>
        </ul>
        <div class='clearfix'></div>
        <div class="tab-content">
            <div id="tab1" class="tab-pane active">
                <form class="mt-3">
                    <div class="form-group row">
                        <label for="select" class="col-4 col-form-label">Application for Grade</label>
                        <div class="col-2">
                            <select id="select" name="select" class="custom-select">
                                <option value="rabbit">10</option>
                                <option value="duck">11</option>
                                <option value="fish">12</option>
                            </select>
                        </div>
                        <label class="col-3 col-form-label" for="text">Academic Year</label>
                        <div class="col-2">
                            <input id="text" name="text" value="2018/2019"
                            placeholder="2018/2019" type="text" class="form-control"
                                required="required">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="text1" class="col-4 col-form-label">First Name</label>
                        <div class="col-4">
                            <input id="text1" name="text1" type="text" class="form-control">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="text2" class="col-4 col-form-label">Last Name</label>
                        <div class="col-4">
                            <input id="text2" name="text2" type="text" class="form-control">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="text3" class="col-4 col-form-label">Nickname</label>
                        <div class="col-4">
                            <input id="text3" name="text3" type="text" class="form-control">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="text4" class="col-4 col-form-label">Birth Place</label>
                        <div class="col-4">
                            <input id="text4" name="text4" type="text" class="form-control">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="text4" class="col-4 col-form-label">Date Of Birth</label>
                        <div class="col-4">
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <div class="input-group-text">
                                        <i class="mdi mdi-calendar-range"></i>
                                    </div>
                                </div>
                                <input id="text4" name="text4" type="text" class="form-control">
                            </div>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="text6" class="col-4 col-form-label">Nationality</label>
                        <div class="col-4">
                            <input id="text6" name="text6" type="text" class="form-control">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="text7" class="col-4 col-form-label">Passport Number</label>
                        <div class="col-4">
                            <input id="text7" name="text7" type="text" class="form-control">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="text8" class="col-4 col-form-label">Religion</label>
                        <div class="col-4">
                            <input id="text8" name="text8" type="text" class="form-control">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="text8" class="col-4 col-form-label">Home Phone</label>
                        <div class="col-4">
                            <input id="text9" name="text9" type="text" class="form-control">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="text10" class="col-4 col-form-label">Mobile Phone</label>
                        <div class="col-4">
                            <input id="text10" name="text10" type="text" class="form-control">
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="offset-4 col-8">
                            <button name="submit" type="submit" class="btn btn-primary">Submit</button>
                        </div>
                    </div>
                </form>
            </div>
            <div id="tab2" class="tab-pane">
                <form class="mt-3">
                    <div class="form-group row">
                        <label class="col-4">Student lives with</label>
                        <div class="col-8">
                            <div class="custom-control custom-checkbox custom-control-inline">
                                <input name="checkbox" id="checkbox_0" type="checkbox" checked="checked"
                                    class="custom-control-input" value="rabbit" aria-describedby="checkboxHelpBlock">
                                <label for="checkbox_0" class="custom-control-label">Parents</label>
                            </div>
                            <div class="custom-control custom-checkbox custom-control-inline">
                                <input name="checkbox" id="checkbox_1" type="checkbox" class="custom-control-input"
                                    value="duck" aria-describedby="checkboxHelpBlock">
                                <label for="checkbox_1" class="custom-control-label">Grandparents</label>
                            </div>
                            <div class="custom-control custom-checkbox custom-control-inline">
                                <input name="checkbox" id="checkbox_2" type="checkbox" class="custom-control-input"
                                    value="fish" aria-describedby="checkboxHelpBlock">
                                <label for="checkbox_2" class="custom-control-label">Other</label>
                            </div>
                            <span id="checkboxHelpBlock" class="form-text text-muted">(Check any that apply)</span>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="text1" class="col-4 col-form-label">Name of Guardian(s) with whom student resides if
                            other than parents</label>
                        <div class="col-8">
                            <input id="text1" name="text1" type="text" class="form-control">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-4">The custodial parent</label>
                        <div class="col-8">
                            <div class="custom-control custom-radio custom-control-inline">
                                <input name="radio" id="radio_0" type="radio" class="custom-control-input"
                                    value="rabbit" aria-describedby="radioHelpBlock">
                                <label for="radio_0" class="custom-control-label">Father</label>
                            </div>
                            <div class="custom-control custom-radio custom-control-inline">
                                <input name="radio" id="radio_1" type="radio" class="custom-control-input" value="duck"
                                    aria-describedby="radioHelpBlock">
                                <label for="radio_1" class="custom-control-label">Mother</label>
                            </div>
                            <span id="radioHelpBlock" class="form-text text-muted">(If parents are divorced)</span>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-12">
                            <p>Other Children (List from eldest to youngest, including the applicant’s name in the
                                appropriate position).</p>
                            <div class="float-sm-right">
                                <button type="button" class="btn btn-primary btn-sm">Add</button>
                            </div>
                            <div class="clearfix"></div>
                            <table class="table table-bordered">
                                <thead>
                                    <tr>
                                        <th>Name</th>
                                        <th>Birth Of Date</th>
                                        <th>Gender</th>
                                        <th>Grade</th>
                                        <th>Currently residing</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>&nbsp;</td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                    </tr>
                                </tbody>

                            </table>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-12">
                            <p>List of people other than parents that can be contacted in case of emergency:</p>
                            <div class="float-sm-right">
                                <button type="button" class="btn btn-primary btn-sm">Add</button>
                            </div>
                            <div class="clearfix"></div>
                            <table class="table table-bordered">
                                <thead>
                                    <tr>
                                        <th>Name</th>
                                        <th>Relation to Child</th>
                                        <th>Telephone Number</th>

                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>&nbsp;</td>
                                        <td></td>
                                        <td></td>

                                    </tr>
                                </tbody>

                            </table>
                        </div>
                    </div>

                    <div class="form-group row">
                        <div class="offset-4 col-8">
                            <button name="submit" type="submit" class="btn btn-primary">Submit</button>
                        </div>
                    </div>
                </form>
            </div>
            <div id="tab3" class="tab-pane">
                <form class="mt-3">
                    <div class="row">
                        <div class="col-12">
                            <p>Former Schools, if applicable (attach copies of Report Cards, Diploma/School
                                Certificates, Reference Letters, etc.)</p>
                            <div class="float-sm-right">
                                <button type="button" class="btn btn-primary btn-sm">Add</button>
                            </div>
                            <div class="clearfix"></div>
                            <table class="table table-bordered">
                                <thead>
                                    <tr>
                                        <th>School</th>
                                        <th>School Name & Location</th>
                                        <th>Years Attended</th>
                                        <th>Grades Attended</th>
                                        <th>Language of Instruction</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>Preschool</td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                    </tr>
                                    <tr>

                                        <td>Kindergarten</td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                    </tr>
                                    <tr>

                                        <td>Elementary</td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                    </tr>
                                    <tr>

                                        <td>Middle School</td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                    </tr>
                                </tbody>

                            </table>
                        </div>
                    </div>
                    <div class="form-group">
                        <label>How did you first hear about Sekolah HighScope Indonesia?</label>
                        <div class="row">
                            <div class="col-4">
                                <div class="custom-control custom-checkbox custom-control-inline">
                                    <input name="checkbox" id="checkbox_0" type="checkbox" checked="checked"
                                        class="custom-control-input" value="">
                                    <label for="checkbox_0" class="custom-control-label">Admission
                                        Representative</label>
                                </div>
                            </div>
                            <div class="col-4">
                                <div class="custom-control custom-checkbox custom-control-inline">
                                    <input name="checkbox" id="checkbox_1" type="checkbox" class="custom-control-input"
                                        value="">
                                    <label for="checkbox_1" class="custom-control-label">Current Student</label>
                                </div>
                            </div>
                            <div class="col-4">
                                <div class="custom-control custom-checkbox custom-control-inline">
                                    <input name="checkbox" id="checkbox_2" type="checkbox" class="custom-control-input"
                                        value="">
                                    <label for="checkbox_2" class="custom-control-label">Advertisement</label>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-4">
                                <div class="custom-control custom-checkbox custom-control-inline">
                                    <input name="checkbox" id="checkbox_3" type="checkbox" class="custom-control-input"
                                        value="">
                                    <label for="checkbox_3" class="custom-control-label">Catalog</label>
                                </div>
                            </div>
                            <div class="col-4">
                                <div class="custom-control custom-checkbox custom-control-inline">
                                    <input name="checkbox" id="checkbox_4" type="checkbox" class="custom-control-input"
                                        value="">
                                    <label for="checkbox_4" class="custom-control-label">Alumnus</label>
                                </div>
                            </div>
                            <div class="col-4">
                                <div class="custom-control custom-checkbox custom-control-inline">
                                    <input name="checkbox" id="checkbox_5" type="checkbox" class="custom-control-input"
                                        value="">
                                    <label for="checkbox_5" class="custom-control-label">Teacher</label>
                                </div>

                            </div>
                        </div>
                        <div class="row">
                            <div class="col-4">
                                <div class="custom-control custom-checkbox custom-control-inline">
                                    <input name="checkbox" id="checkbox_6" type="checkbox" class="custom-control-input"
                                        value="">
                                    <label for="checkbox_6" class="custom-control-label">Open House</label>
                                </div>
                            </div>
                            <div class="col-4">
                                <div class="custom-control custom-checkbox custom-control-inline">
                                    <input name="checkbox" id="checkbox_7" type="checkbox" class="custom-control-input"
                                        value="">
                                    <label for="checkbox_7" class="custom-control-label">School Member</label>
                                </div>
                            </div>
                            <div class='col-4'>
                                <div class="custom-control custom-checkbox custom-control-inline">
                                    <input name="checkbox" id="checkbox_8" type="checkbox" class="custom-control-input"
                                        value="">
                                    <label for="checkbox_8" class="custom-control-label">Other</label>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="textarea">List other relatives who currently attend or have previously attended
                            Sekolah HighScope Indonesia:</label>
                        <textarea id="textarea" name="textarea" cols="40" rows="5" class="form-control"></textarea>
                    </div>
                    <div class="form-group">
                        <label for="textarea1">Other schools to which you are considering to apply:</label>
                        <textarea id="textarea1" name="textarea1" cols="40" rows="5" class="form-control"></textarea>
                    </div>
                    <div class="form-group row">
                        <div class="offset-4 col-8">
                            <button name="submit" type="submit" class="btn btn-primary">Submit</button>
                        </div>
                    </div>
                </form>
            </div>
            <div id="tab4" class="tab-pane">

                <form class="mt-3">
                    <h3>Please Upload supporting documents as required below</h3>
                    <div class="form-group mt-3">
                        <label for="text">Photograph Of Applicant</label>
                        <div class="custom-file">
                            <input type="file" class="custom-file-input" id="customFile">
                            <label class="custom-file-label" for="customFile">Choose file</label>
                        </div>
                    </div>
                    <div class="form-group mt-3">
                        <label for="text">Report Card From Former School</label>
                        <div class="custom-file">
                            <input type="file" class="custom-file-input" id="customFile">
                            <label class="custom-file-label" for="customFile">Choose file</label>
                        </div>
                    </div>
                    <div class="form-group mt-3">
                        <label for="text">Diploma / Certificates</label>
                        <div class="custom-file">
                            <input type="file" class="custom-file-input" id="customFile">
                            <label class="custom-file-label" for="customFile">Choose file</label>
                        </div>
                        <div class="custom-file">
                            <input type="file" class="custom-file-input" id="customFile">
                            <label class="custom-file-label" for="customFile">Choose file</label>
                        </div>
                        <div class="custom-file">
                            <input type="file" class="custom-file-input" id="customFile">
                            <label class="custom-file-label" for="customFile">Choose file</label>
                        </div>
                    </div>
                    <div class="form-group mt-3">
                        <label for="text">Reference Letters</label>
                        <div class="custom-file">
                            <input type="file" class="custom-file-input" id="customFile">
                            <label class="custom-file-label" for="customFile">Choose file</label>
                        </div>
                        <div class="custom-file">
                            <input type="file" class="custom-file-input" id="customFile">
                            <label class="custom-file-label" for="customFile">Choose file</label>
                        </div>
                        <div class="custom-file">
                            <input type="file" class="custom-file-input" id="customFile">
                            <label class="custom-file-label" for="customFile">Choose file</label>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="offset-4 col-8">
                            <button name="submit" type="submit" class="btn btn-primary">Submit</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>

    </div>
</div>