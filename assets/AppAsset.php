<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\assets;

use yii\web\AssetBundle;

/**
 * Main application asset bundle.
 *
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class AppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'https://fonts.googleapis.com/css?family=Montserrat:400,500|Poppins:400,500,600,700|Roboto:400,500',
        'https://cdn.materialdesignicons.com/3.0.39/css/materialdesignicons.min.css',
        '/css/sleek.css',
        '/plugins/nprogress/nprogress.css'

    ];
    public $js = [
        '/plugins/nprogress/nprogress.js',
        '/plugins/jekyll-search.min.js',
        '/plugins/slimscrollbar/jquery.slimscroll.min.js',
        '/js/sleek.bundle.js',

    ];
    public $depends = [
        'yii\web\YiiAsset',
      //  'yii\bootstrap4\BootstrapPluginAsset',
    ];
}
