<div class="card card-default">
    <div class="row no-gutter">
        <div class="col-xl-4">
            <div class="card-body">
                <img src="/img/student.png" class="user-image mx-auto d-block" alt="User Image" />
            </div>
        </div>
        <div class="col-xl-8">
            <div class="card-body">
                <dl classs="row">
                    <dt class="col-sm-4">Grade</dt>
                    <dd class="col-sm-8">10</dd>
                    <dt class="col-sm-4">Name</dt>
                    <dd class="col-sm-8">Donny Orlando</dd>
                    <dt class="col-sm-4">Major</dt>
                    <dd class="col-sm-8">Business And Information Technology</dd>
                </dl>
            </div>
        </div>
        
    </div>
</div>