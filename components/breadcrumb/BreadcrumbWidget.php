<?php
namespace app\components\breadcrumb;
use yii\base\Widget;
use yii\helpers\Html;

class BreadcrumbWidget extends Widget
{
    public function init()
    {
        parent::init();
    }

    public function run()
    {
        
        return $this->render('breadcrumb');
    }
}
?>

