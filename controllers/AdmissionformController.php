<?php

namespace app\controllers;

class AdmissionformController extends \yii\web\Controller
{
    public function actionHighschool()
    {
        $this->layout="admissionlayout";
        return $this->render('highschool');
    }
    public function actionIndex(){
        $this->layout="admissionlayout";
        return $this->render('index');
    }

}
