<?php

namespace app\controllers;

class ClassroomController extends \yii\web\Controller
{
    public function actionIndex()
    {
        return $this->render('index');
    }
    public function actionClass($classname=null){
        return $this->render('classroom');
    }

    public function actionReportcard(){
        return $this->render('reportcard');
    }

}
